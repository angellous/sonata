<?php

namespace Acme\CategoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
// use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category
 *
 * @ORM\Table("tbl_categories")
 * @ORM\Entity(repositoryClass="Acme\CategoryBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category")
     */
    protected $products;

    /**
     * @var text
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="des", type="text")
     */
    private $des;

    /**
     * @Gedmo\Timestampable(on="create")
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    // private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     */
    // private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10, nullable=true)
     */
    private $status=0;


    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set des
     *
     * @param string $des
     * @return Category
     */
    public function setDes($des)
    {
        $this->des = $des;

        return $this;
    }

    /**
     * Get des
     *
     * @return string 
     */
    public function getDes()
    {
        return $this->des;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Category
     */
    // public function setCreatedAt($createdAt)
    // {
    //     $this->createdAt = $createdAt;

    //     return $this;
    // }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    // public function getCreatedAt()
    // {
    //     return $this->createdAt;
    // }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Category
     */
    // public function setUpdatedAt($updatedAt)
    // {
    //     $this->updatedAt = $updatedAt;

    //     return $this;
    // }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    // public function getUpdatedAt()
    // {
    //     return $this->updatedAt;
    // }

    /**
     * Set status
     *
     * @param string $status
     * @return Category
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add products
     *
     * @param \Acme\CategoryBundle\Entity\Product $products
     * @return Category
     */
    public function addProduct(\Acme\CategoryBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Acme\CategoryBundle\Entity\Product $products
     */
    public function removeProduct(\Acme\CategoryBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function __toString()
    {
        return $this->name;
    }
}
