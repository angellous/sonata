<?php
namespace Acme\DemoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Sonata\AdminBundle\Show\ShowMapper;

class PostAdmin extends Admin
{

    protected $baseRouteName = 'sonata_post';
    protected $baseRoutePattern = 'post';
// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->tab('General')
        ->with('',
            array(
                'class'       => 'col-md-12',
                'description' => '',
                ))
        ->add('title', 'text', array('label' => 'Post Title'))
        ->add('body') 
        // ->add('publish','boolean') 
        ->add('createdBy', 'text')
        ->end()
        ->end()
        ->tab('Meta Information') // the tab call is optional
        ->with('Addresses',
            array(
                'class'       => 'col-md-12',
                'description' => 'Lorem ipsum',
                ))
        ->end()
        ->end()
        ;


        $formMapper
        ->with('General')
        ->add('title', 'text', array('label' => 'Post Title'))
        ->add('body') 
        ->end()
        ->with('Generala')
        ->add('title', 'text', array('label' => 'Post Title'))
        ->add('body') 
        ->end();

/*        $formMapper
        ->with('General', array('description' => 'This section contains general settings for the web page'))
        ->add('title', null, array('help'=>'Set the title of a web page'))
        ->add('body', null, array('help'=>'Set the body of a web page'))
        ->end();
*/        
    }

// Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ->add('title')
        ->add('body')
        ->add('publish')
        ;
    }

// Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        ->addIdentifier('title')
        ->add('body')
        ->add('publish')
        ->add('_action', 'actions', array('label'=>'Actions',
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
                )
            ));
    }

    public function getBatchActions(){
        $actions=parent::getBatchActions();
        $actions['unpublish']=array('label'=>'Unpublish','ask_confirmation'=>true);
        return $actions;
    }

// public function createQuery($context = 'list')
// {
//     $query = parent::createQuery($context);
//     $query->andWhere(
//         $query->expr()->eq($query->getRootAliases()[0] . '.title', ':my_param')
//         );
//     $query->setParameter('my_param', 'my_value');
//     return $query;
// }

    protected $datagridValues = array(
'_page' => 1,            // display the first page (default = 1)
'_sort_order' => 'DESC', // reverse order (default = 'ASC')
'_sort_by' => 'title'  // name of the ordered field
// (default = the model's id field, if any)
// the '_sort_by' key can be of the form 'mySubModel.mySubSubModel.myField'.
);


}