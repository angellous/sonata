<?php
namespace Acme\CategoryBundle\Newsletter;

use Acme\CategoryBundle\Mailer;

class NewsletterManager
{
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    // ...
}