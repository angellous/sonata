<?php

namespace Acme\DemoBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;


class AcmeDemoExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {

        

    }

    public function getAlias()
    {
        return 'acme_demo';
    }
}
