<?php
namespace Acme\CategoryBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Validator\ValidatorInterface;

class ProductAdmin extends Admin
{

    protected $baseRouteName = 'sonata_product';
    protected $baseRoutePattern = 'product';

// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->add('name')
        ->add('category')
        ->add('des')
        ->add('path')
        ->add(
            'status','choice',array(
                'choices' => array(
                    'prep' => 'Prepared',
                    'prog' => 'In progress',
                    'done' => 'Done'
                    )))
        ->end()
        ;
    }

// Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

// Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        ->addIdentifier('name')
        ->add('category')
        ->add('des')
        ->add('path', 'file', array('required' => false))
        ->add('status')
        ->add('_action', 'actions', array('label'=>'Actions',
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
                )
            ));
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
        ->with('name')->assertLength(array('max' => 5))->addViolation('Invaild length')
        ->end()
        ;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        if (
          $this->hasRoute('edit') && $this->isGranted('EDIT') &&
          $this->hasRoute('delete') && $this->isGranted('DELETE')
          ) 
        {
            $actions['merge'] = array(
                'label' => $this->trans('action_merge', array(), 'SonataAdminBundle'),
                'ask_confirmation' => true
                );

        }
        dump($actions);
        return $actions;
    }

}