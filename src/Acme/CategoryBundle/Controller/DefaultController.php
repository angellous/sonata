<?php

namespace Acme\CategoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Acme\CategoryBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/category")
 */

class DefaultController extends Controller
{

   /**
     * @Route("/",name="_category")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AcmeCategoryBundle:Category')->findAll();
        return array('entities' => $entities);
    }

    /**
     * @Route("/show/{id}",name="_category_show")
     * @Template()
     */
    public function readAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Category')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        return $this->render('AcmeCategoryBundle:Default:show.html.twig',array('entity'=>$entity));
    }

    /**
     * @Route("/add",name="_category_add")
     * @Template()
     */
    public function createAction()
    {
        if(isset($_POST['add'])){
            $entity=new Category;
            $entity->setName($_POST['name']);
            $entity->setDes($_POST['des']);
            $entity->setStatus(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('_category'));            
        }
        return $this->render('AcmeCategoryBundle:Default:new.html.twig');
    }

    /**
     * @Route("/edit/{id}",name="_category_edit")
     * @Template()
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Category')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        if(isset($_POST['update'])){
            $entity->setName($_POST['name']);
            $entity->setDes($_POST['des']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('_category'));            
        }
        return $this->render('AcmeCategoryBundle:Default:edit.html.twig',array('entity'=>$entity));
    }

    /**
     * @Route("/delete/{id}",name="_category_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Category')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('_category'));            
    }


}
