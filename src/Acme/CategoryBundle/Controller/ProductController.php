<?php

namespace Acme\CategoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Acme\CategoryBundle\Entity\Product;
use Acme\CategoryBundle\Form\ProductType;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

//acl
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

use Acme\HelloBundle\Newsletter\NewsletterManager;

/**
 * @Route("/product")
 */

class ProductController extends Controller
{
    /**
     * @Route("/",name="_product")
     * @Template()
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$entities = $em->getRepository('AcmeCategoryBundle:Product')->findAll();
        $tokenStorage = $this->get('security.token_storage');
        $user = $tokenStorage->getToken()->getUser();

        return array('entities' => $entities);
    }

    /**
     * @Route("/show/{id}",name="_product_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Product')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        return array('entity'=>$entity);
    }

    /**
     * @Route("/add",name="_product_add")
     * @Template()
     * @Method("GET")
     */
    public function newAction()
    {
        $em=$this->getDoctrine()->getManager();
        $entities=$em->getRepository('AcmeCategoryBundle:Category')->findAll();
        $entity=new Product;
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('_product_add_db'),
            'method' => 'POST',
            ));
        $form->add('submit', 'submit', array('label' => 'Add'));
        return array('form'=>$form->createView(),'entities'=>$entities);
    }

    /**
     * @Route("/add",name="_product_add_db")
     * @Method("POST")
     * @Template()
     */
    public function createDbAction(Request $request)
    {
        $errors=array();
        $em=$this->getDoctrine()->getManager();
        $entities=$em->getRepository('AcmeCategoryBundle:Category')->findAll();
        $entity = new Product();
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('_product_add_db'),
            'method' => 'POST',
            ));
        $form->add('submit', 'submit', array('label' => 'Add'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // creating the ACL
            $aclProvider = $this->get('security.acl.provider');
            $objectIdentity = ObjectIdentity::fromDomainObject($entity);
            $acl = $aclProvider->createAcl($objectIdentity);

            // retrieving the security identity of the currently logged-in user
            $tokenStorage = $this->get('security.token_storage');
            $user = $tokenStorage->getToken()->getUser();
            $securityIdentity = UserSecurityIdentity::fromAccount($user);

            // grant owner access
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);
            return $this->redirect($this->generateUrl('_product'));            
        }
        else{
            // 1
            // $errors = $this->get('validator')->validate($form);
            // 2
            // $errors=$form->getErrorsAsString();
            // 3
            // $errors=$form->getErrors(true);
            // 4
            // $errors=$this->getFormErrors($form);
            // dump($errors);
        }
        return $this->render('AcmeCategoryBundle:Product:new.html.twig',
            array(
                'errors'=>$errors,
                'entities'=>$entities,
                'form'=>$form->createView()
                )
            );
    }

    /**
     * @Route("/edit/{id}",name="_product_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Product')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        $authorizationChecker = $this->get('security.authorization_checker');

        
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $securityIdentity = UserSecurityIdentity::fromAccount($user);

        // check for edit access
        if (false === $authorizationChecker->isGranted('EDIT', $entity)) {
            throw new AccessDeniedException();
        }
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('_product_edit_db', array('id' => $entity->getId())),
            'method' => 'POST',
            ));
        $form->add('submit', 'submit', array('label' => 'Update'));
        return array('form'=>$form->createView());
    }

    /**
     * @Route("/edit/{id}",name="_product_edit_db")
     * @Method("POST")
     */
    public function editDbAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Product')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        $form = $this->createForm(new ProductType(), $entity, array(
            'action' => $this->generateUrl('_product_edit_db', array('id' => $entity->getId())),
            'method' => 'POST',
            ));
        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('_product'));            
        }
        return $this->render('AcmeCategoryBundle:Product:edit.html.twig',
            array(
                'entity'=>$entity,
                'form'=>$form->createView()
                )
            );
    }

    /**
     * @Route("/delete/{id}",name="_product_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeCategoryBundle:Product')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('_product'));            
    }

    public function getFormErrors(Form $form)
    {
        //
        if ($err = $this->childErrors($form)) {
            $errors["form"] = $err;
        }

        //
        foreach ($form->all() as $key => $child) {
            //
            if ($err = $this->childErrors($child)) {
                $errors[$key] = $err;
            }
        }

        return $errors;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     * @return array
     */
    public function childErrors(Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $error) {
            // Переведем ошибку, если есть перевод
            $message = $this->container->get('translator')->trans($error->getMessage(), array(), 'validators');
            array_push($errors, $message);
        }

        return $errors;
    }

    /**
     * @Route("/sendNewsletter/{id}", name="_product_send")
     */
    public function sendNewsletterAction()
    {
        return new Response("send newsletter");
        $mailer = $this->get('my_mailer');
        $newsletter = new NewsletterManager($mailer);
    }


    /**
     * @Route("/sendMail", name="_product_sendmail")
     * @Template()
     */
    public function sendMailAction(){
        $mailer = $this->get('mailer');
        $message = $mailer->createMessage()
        ->setSubject('You have Completed Registration!')
        ->setFrom('send@example.com')
        ->setTo('ramesh.gurung@Acmesoft.com')
        ->setBody('hey body');
        $mailer->send($message);
        return array($mailer);
    }
}
