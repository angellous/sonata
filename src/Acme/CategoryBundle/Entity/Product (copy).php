<?php

namespace Acme\CategoryBundle\Entitey;

use Doctrine\ORM\Mapping as ORM;
// use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table("tbl_products")
 * @ORM\Entity(repositoryClass="Acme\CategoryBundle\Entity\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="des", type="string", length=1000)
     * @Assert\NotBlank()
     */
    private $des;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean",nullable=true)
     */
    private $status=0;


    /**
     * @Gedmo\Timestampable(on="create")
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    // private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     */
    // private $updatedAt;

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Category
     */
    // public function setCreatedAt($createdAt)
    // {
    //     $this->createdAt = $createdAt;

    //     return $this;
    // }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    // public function getCreatedAt()
    // {
    //     return $this->createdAt;
    // }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Category
     */
    // public function setUpdatedAt($updatedAt)
    // {
    //     $this->updatedAt = $updatedAt;

    //     return $this;
    // }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    // public function getUpdatedAt()
    // {
    //     return $this->updatedAt;
    // }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set des
     *
     * @param string $des
     * @return Product
     */
    public function setDes($des)
    {
        $this->des = $des;

        return $this;
    }

    /**
     * Get des
     *
     * @return string 
     */
    public function getDes()
    {
        return $this->des;
    }

     /**
     * Set status
     *
     * @param boolean $status
     * @return Product
     */
     public function setStatus($status)
     {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set category
     *
     * @param \Acme\CategoryBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\Acme\CategoryBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Acme\CategoryBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
    public function __toString()
    {
        return $this->name;
    }

}
