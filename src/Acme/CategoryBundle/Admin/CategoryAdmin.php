<?php
namespace Acme\CategoryBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CategoryAdmin extends Admin
{

    protected $baseRouteName = 'sonata_category';
    protected $baseRoutePattern = 'category';
    public $supportsPreviewMode = true;

// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
         $imageFieldOptions = array(); // see available options below
         $formMapper
         ->add('name')
         ->add('des')
         ->setHelps(array(
            'name' => 'Please provide unique name ',
            'des' => 'Please provide informative description',
            ))
         // ->add('image1', 'sonata_type_model', $imageFieldOptions)
         ->end()
         ;
     }

// Fields to be shown on filter forms
     protected function configureDatagridFilters(DatagridMapper $datagridMapper)
     {
     }

// Fields to be shown on lists
     protected function configureListFields(ListMapper $listMapper)
     {
        $listMapper
        ->addIdentifier('name')
        ->add('des')
        ->add('status')
        ->add('_action', 'actions', array('label'=>'Actions',
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array(),
                // 'new product' => array(),
                )
            ));
    }


}